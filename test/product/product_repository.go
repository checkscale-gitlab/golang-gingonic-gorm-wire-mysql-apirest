package product

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" //mysql
)

// ProductRepository : obtain the context of the database
type ProductRepository struct {
	DB *gorm.DB
}

// ProvideProductRepostiory : provides the repository to use the db context
func ProvideProductRepostiory(DB *gorm.DB) ProductRepository {
	return ProductRepository{DB: DB}
}

//FindAll : obtain from db all the data of the indicated gorm model
func (p *ProductRepository) FindAll() []Product {
	var products []Product
	p.DB.Find(&products)

	return products
}

//FindByID :  obtain from db the data that contains the param id
func (p *ProductRepository) FindByID(id uint) Product {
	var product Product
	p.DB.First(&product, id)

	return product
}

//Save : stores the changes occurred when creating or updating entries in the database
func (p *ProductRepository) Save(product Product) Product {
	p.DB.Save(&product)

	return product
}

// Delete : Deletes from the data base
func (p *ProductRepository) Delete(product Product) {
	p.DB.Delete(&product)
}
