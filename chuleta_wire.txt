creamos el fichero wire.go
( el propósito del inyector es proporcionar información sobre qué proveedores usar para construir un Evento y, 
por lo tanto, lo excluiremos de nuestro binario final con una restricción de compilación en la parte superior del archivo:)

//+build wireinject  

package main

import (
	"Go_Gingonic_Server/product"

	"github.com/google/wire"
	"github.com/jinzhu/gorm"
)

func initProductAPI(db *gorm.DB) product.ProductAPI {
	wire.Build(product.ProvideProductRepostiory, product.ProvideProductService, product.ProvideProductAPI)

	return product.ProductAPI{}
}